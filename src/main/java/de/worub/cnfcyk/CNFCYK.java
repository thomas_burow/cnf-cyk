package de.worub.cnfcyk;

import com.google.common.collect.Lists;
import com.google.gson.*;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

public class CNFCYK {
    private static String getSentence(CommandLine cl) {
        StringBuilder sb = new StringBuilder();
        for (String text : cl.getArgList() ) {
            sb.append(text);
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    private class ElementSerializer implements JsonSerializer<Element> {
        public JsonElement serialize(Element src, Type typeOfSrc, JsonSerializationContext context) {
            if (src.isTerminal()) {
                return new JsonPrimitive(src.toString().toLowerCase());
            } else {
                return new JsonPrimitive(src.toString().toUpperCase());
            }
        }
    }

    private class ElementDeserializer implements JsonDeserializer<Element> {
        public Element deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            try {
                String name = json.getAsJsonPrimitive().getAsString();
                String lowerCaseName = name.toLowerCase();

                if (name.equals(lowerCaseName)) {
                    return new Terminal(lowerCaseName);
                } else {
                    return new NonTerminal(lowerCaseName);
                }
            } catch (Exception e) {
                return null;
            }
        }
    }

    private class RuleSerializer implements JsonSerializer<Rule> {
        public JsonElement serialize(Rule src, Type typeOfSrc, JsonSerializationContext context) {
            ElementSerializer serializer = new ElementSerializer();
            JsonObject object = new JsonObject();
            JsonArray array = new JsonArray();
            for (Element element : src.getRightSide()) {
                array.add(serializer.serialize(element, Element.class, context));
            }
            object.add(src.getLeftSide().toString().toUpperCase(), array);
            return object;
        }
    }

    private class RuleDeserializer implements JsonDeserializer<Rule> {
        public Rule deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            ElementDeserializer deserializer = new ElementDeserializer();
            String key = json.getAsJsonObject().entrySet().iterator().next().getKey();
            String lowerCaseKey = key.toLowerCase();

            try {
                Iterator<JsonElement> iterator = json.getAsJsonObject().get(key).getAsJsonArray().iterator();
                List<Element> list = Lists.newArrayList();
                while (iterator.hasNext()) {
                    list.add(deserializer.deserialize(iterator.next(), Element.class, context));
                }
                return new Rule(new NonTerminal(lowerCaseKey), list);
            } catch (Exception e) {
                return null;
            }
        }
    }

    private RuleSet loadFromFile(File file) {
        RuleSet ruleSet = new RuleSet();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Element.class, new ElementSerializer());
        gsonBuilder.registerTypeAdapter(Element.class, new ElementDeserializer());
        gsonBuilder.registerTypeAdapter(Rule.class, new RuleSerializer());
        gsonBuilder.registerTypeAdapter(Rule.class, new RuleDeserializer());
        Gson gson = gsonBuilder.create();

        try {
            Rule[] list = gson.fromJson(new FileReader(file), Rule[].class);
            for (Rule rule : list) {
                ruleSet.addRule(rule);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return ruleSet;
    }

    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("f", "file",  true,  "The file which will be loaded contains the rule set for which the cnf and cyk algorithms will be applied. This is a mandatory argument.");
        options.addOption("s", "start", true,  "The <arg> defines the start symbol. Which has to be present in the file. This is a mandatory argument.");
        options.addOption("t", "term",  false, "Shows the rule set after rules with nonsolitary terminals are eliminated.");
        options.addOption("b", "bin",   false, "Shows the rule set after right-hand sides with more than 2 nonterminals are eliminated.");
        options.addOption("d", "del",   false, "Shows the rule set after empty rules are eliminated.");
        options.addOption("u", "unit",  false, "Shows the rule set after unit rules are eliminated. The rule set is now in Chomsky normal form.");
        options.addOption("h", "Prints this message.");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        String sentence = getSentence(cmd);

        if (cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "CNFCYK -bdhtu -f <file> -s <startElement> <sentence>",
                    "\nThis program can be used to check wether a sentence belongs to a certain grammar. This is" +
                    " accomplished by using the CYK algorithm. In order to use the CYK algorithm, you need your" +
                    " grammar to be in Chomsky Normal Form (CNF). This program will provide both parts. First it" +
                    " brings your grammar into CNF and after that it will check wether your sentence belongs to the" +
                    " grammar or not.\n\n" +
                    "The file which carries the grammar has a special format. All non terminals are in upper case and" +
                    " all terminals are in lower case. For an empty element use [].\n\n" +
                    "[\n" +
                    "  {\"S\":[\"A\",\"B\"]},\n" +
                    "  {\"A\":[\"a\"]},\n" +
                    "  {\"B\":[]},\n" +
                    "]\n\n",
                    options, "", false);
            System.exit(0);
        }

        if (cmd.hasOption("f") && sentence.length() > 0 && cmd.hasOption("s")) {
            String filename = cmd.getOptionValue("file");
            String startElement = cmd.getOptionValue("s");

            File file = new File(filename);
            if(file.exists() && !file.isDirectory()) {
                RuleSet ruleSet = null;
                try {
                    ruleSet = new CNFCYK().loadFromFile(file);
                } catch (Exception e) {
                    System.out.println("File could not be loaded");
                    System.exit(1);
                }

                ruleSet.term();
                if (cmd.hasOption("t")) {
                    System.out.println("After Term:");
                    System.out.println(ruleSet);
                }
                ruleSet.bin();
                if (cmd.hasOption("b")) {
                    System.out.println("After Bin:");
                    System.out.println(ruleSet);
                }
                ruleSet.del();
                if (cmd.hasOption("d")) {
                    System.out.println("After Del:");
                    System.out.println(ruleSet);
                }
                ruleSet.unit();
                if (cmd.hasOption("u")) {
                    System.out.println("After Unit (CNF):");
                    System.out.println(ruleSet);
                }

                System.out.println(ruleSet.cyk(sentence, startElement));
            } else {
                System.out.println("File (" + filename + ") does not exist.");
                System.exit(2);
            }
        }
    }
}
