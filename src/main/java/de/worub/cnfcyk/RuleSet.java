package de.worub.cnfcyk;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

class RuleSet {
    private Map<NonTerminal, List<Rule>> ruleMap = Maps.newHashMap();;
    private List<NonTerminal> nonTerminals = Lists.newArrayList();
    private List<Rule> rules = Lists.newArrayList();

    public RuleSet() {
    }

    public boolean cyk(String sentence, String... start) throws Exception {
        String[] parts = sentence.split(" ");

        int n = parts.length;
        int r = this.rules.size();

        boolean[][][] p = new boolean[n + 1][n + 1][r + 1];

        for (int i = 1; i <= n; i++) {
            Element part = new Terminal(parts[i - 1]);
            for (int j = 1; j <= r; j++) {
                if (this.rules.get(j - 1).getRightSide().size() == 1) {
                    Element element = this.rules.get(j - 1).getRightSide().get(0);
                    if (part.equals(element)) {
                        p[1][i][j] = true;
                    }
                }
            }
        }

        Map<Element, List<Integer>> helper = Maps.newHashMap();
        for (int x = 1; x <= r; x++) {
            Rule rule = this.rules.get(x - 1);
            List<Integer> list = helper.get(rule.getLeftSide());
            if (list == null) {
                list = Lists.newArrayList();
                helper.put(rule.getLeftSide(), list);
            }
            list.add(x);
        }

        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= (n - i + 1); j++) {
                for (int k = 1; k <= i - 1; k++) {
                    for (int l = 1; l <= r; l++) {
                        Rule rule = this.rules.get(l - 1);
                        if (rule.getRightSide().size() == 2) {
                            Element elementB = rule.getRightSide().get(0);
                            Element elementC = rule.getRightSide().get(1);

                            for (Integer b : helper.get(elementB)) {
                                for (Integer c : helper.get(elementC)) {
                                    if (p[k][j][b] && p[i - k][j + k][c]) {
                                        p[i][j][l] = true;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        for (int y = 0; y < start.length; y++) {
            List<Integer> startIndex = helper.get(new NonTerminal(start[y]));
            for (int x = 1; x <= startIndex.size(); x++) {
                if (p[n][1][startIndex.get(x - 1)]) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean unitRound() {
        for (Rule rule : this.rules) {
            if (rule.getRightSide().size() == 1 && rule.getRightSide().get(0) instanceof NonTerminal) {
                this.removeRule(rule);
                List<Rule> rules = ruleMap.get(rule.getRightSide().get(0));
                if (rules != null) {
                    for (Rule transRule : rules) {
                        this.addRule(new Rule(rule.getLeftSide(), transRule.getRightSide()));
                    }
                }
                return true;
            }
        }
        return false;
    }

    public void unit() {
        while (unitRound()) {
        }
    }
    
    private boolean hasRuleWithLeftSide(NonTerminal leftSide) {
        for (Rule rule : this.rules) {
            if (rule.getLeftSide().equals(leftSide)) {
                return true;
            }
        }
        return false;
    }

    private boolean delRound() {

        int size = rules.size();
        Rule rule = null;

        // Look for com.worub.Rule like A -> ε
        for (int x = 0; x < size; x++) {
            if (rules.get(x).getRightSide().size() == 0) {
                rule = rules.get(x);
                break;
            }
        }

        if (rule != null) {
            // Found a rule and remove it
            this.removeRule(rule);

            boolean moreRulesWithLeftSide = hasRuleWithLeftSide(rule.getLeftSide());

            List<Rule> emptyRules = Lists.newArrayList();
            for (Rule adjustRule : this.rules) {
                if (adjustRule.getRightSide().contains(rule.getLeftSide())) {
                    emptyRules.add(adjustRule);
                }
            }

            for (Rule removeRule : emptyRules) {
                if (!moreRulesWithLeftSide) {
                    this.removeRule(removeRule);
                }

                if (removeRule.getRightSide().size() == 1) {
                    this.addRule(new Rule(removeRule.getLeftSide()));
                } else if (removeRule.getRightSide().size() == 2) {
                    if (removeRule.getRightSide().get(0).equals(rule.getLeftSide()) && removeRule.getRightSide().get(1).equals(rule.getLeftSide())) {
                        this.addRule(new Rule(removeRule.getLeftSide()));
                    } else if (removeRule.getRightSide().get(0).equals(rule.getLeftSide())) {
                        this.addRule(new Rule(removeRule.getLeftSide(), removeRule.getRightSide().get(1)));
                    } else {
                        this.addRule(new Rule(removeRule.getLeftSide(), removeRule.getRightSide().get(0)));
                    }
                }
            }

            return true;
        }

        return false;
    }

    public void del() {
        while (delRound() == true) {
        }
    }

    public void bin() throws Exception {
        for (int y = 0; y < rules.size(); y++) {
            Rule rule = rules.get(y);
            List<Element> rightSide = rule.getRightSide();
            int size = rightSide.size();
            if (size > 2) {
                List<Element> list = Lists.newArrayList(rightSide);

                NonTerminal nonTerminal = new NonTerminal(rule.getLeftSide());
                int suffix = 0;
                while (this.nonTerminals.contains(nonTerminal)) {
                    suffix++;
                    nonTerminal = new NonTerminal(rule.getLeftSide() + "_" + suffix);
                }
                this.addRule(new Rule(nonTerminal, rightSide.get(size - 2), rightSide.get(size - 1)));
                for (int x = size - 3; x > 0; x--) {
                    suffix++;
                    NonTerminal nextNonTerminal = new NonTerminal(rule.getLeftSide() + "_" + suffix);
                    this.addRule(new Rule(nextNonTerminal, rightSide.get(x), nonTerminal));
                    nonTerminal = nextNonTerminal;
                }
                for (int x = size - 1; x > 0; x--) {
                    rightSide.remove(x);
                }
                rightSide.add(nonTerminal);
            }
        }
    }

    private boolean termRound() throws Exception {
        for (Rule rule : this.rules) {
            List<Element> rightSide = rule.getRightSide();
            int size = rightSide.size();
            if (size > 1) {
                for (int x = 0; x < size; x++) {
                    Element element = rightSide.get(x);
                    if (element instanceof Terminal) {
                        NonTerminal nonTerminal = new NonTerminal(element);
                        int suffix = 0;
                        while (nonTerminals.contains(nonTerminal)) {
                            boolean foundSame = true;
                            for (Rule r : ruleMap.get(nonTerminal)) {
                                if (r.getRightSide().size() != 1 || !r.getRightSide().get(0).equals(element)) {
                                    foundSame = false;
                                }
                            }
                            if (foundSame) {
                                break;
                            }
                            suffix++;
                            nonTerminal = new NonTerminal(element + "_" + suffix);
                        }
                        Rule newRule = new Rule(nonTerminal, element);
                        if (!this.rules.contains(newRule)) {
                            this.addRule(newRule);
                        }
                        rightSide.remove(x);
                        rightSide.add(x, nonTerminal);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void term() throws Exception {
        while (termRound()) {
        }
    }

    public void removeRule(Rule rule) {
        List<Rule> rules = ruleMap.get(rule.getLeftSide());
        if (rules != null) {
            rules.remove(rule);
            if (rules.size() == 0) {
                ruleMap.remove(rule.getLeftSide());
                nonTerminals.remove(rule.getLeftSide());
            }
        }
        this.rules.remove(rule);
    }

    public void addRule(Rule rule) {
        List<Rule> rules = ruleMap.get(rule.getLeftSide());
        if (rules == null) {
            ruleMap.put(rule.getLeftSide(), Lists.newArrayList(rule));
        } else {
            rules.add(rule);
        }
        if (!nonTerminals.contains(rule.getLeftSide())) {
            nonTerminals.add(rule.getLeftSide());
        }
        this.rules.add(rule);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (NonTerminal left : nonTerminals) {
            sb.append(left.print());
            sb.append(" -> ");
            boolean first = true;
            for (Rule rule : ruleMap.get(left)) {
                if (!first) {
                    sb.append("| ");
                }
                first = false;
                if (rule.getRightSide().size() == 0) {
                    sb.append("EMPTY");
                }
                for (Element element : rule.getRightSide()) {
                    sb.append(element.print());
                    sb.append(" ");
                }
            }
            sb.append("\n");
        }
        return sb.toString().trim();
    }
}
