package de.worub.cnfcyk;

class NonTerminal extends Element {
    public NonTerminal(String name) throws Exception {
        super(name, false);
    }

    public NonTerminal(Element terminal) throws Exception {
        super(terminal.toString(), false);
    }
}
