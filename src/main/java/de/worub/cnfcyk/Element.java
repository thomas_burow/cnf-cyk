package de.worub.cnfcyk;

class Element {
    private String name = "";
    private boolean terminal = false;

    public Element(String name, boolean terminal) throws Exception {
        if (name == null || name.trim().length() == 0) {
            throw new Exception("name of Element cannot be empty");
        }
        this.terminal = terminal;
        this.name = name.trim();
    }

    public String print() {
        if (terminal) {
            return name;
        } else {
            return "<" + name + ">";
        }
    }

    public String toString() {
        return name;
    }

    public boolean isTerminal() {
        return terminal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Element element = (Element) o;

        if ("".equals(name) && name.equals(element.name)) return true;
        if (terminal != element.terminal) return false;
        return name.equals(element.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result;
        if (!name.equals("")) {
            result += (terminal ? 1 : 0);
        }
        return result;
    }
}
