package de.worub.cnfcyk;

import com.google.common.collect.Lists;

import java.util.List;

class Rule {
    private NonTerminal left;
    private List<Element> right;

    public Rule(NonTerminal leftSide, Element... elements) {
        this.left = leftSide;
        this.right = Lists.newArrayList(elements);
    }

    public Rule(NonTerminal leftSide, List<Element> elements) {
        this.left = leftSide;
        this.right = Lists.newArrayList(elements);
    }

    public NonTerminal getLeftSide() {
        return left;
    }

    public List<Element> getRightSide() {
        return right;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(left.print());
        sb.append(" -> ");
        if (right.size() == 0) {
            sb.append("ε");
        }
        for (Element element : right) {
            sb.append(element.print());
            sb.append(" ");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        if (!left.equals(rule.left)) return false;
        return right.equals(rule.right);
    }

    @Override
    public int hashCode() {
        int result = left.hashCode();
        result = 31 * result + right.hashCode();
        return result;
    }
}
